let tags = [];
let attempted = 0;
let successful = 0;
const upload = (
    code,
    sha,
    msg,
) => {
    let data = {
        message: msg,
        content: code,
        sha,
    };
    data = JSON.stringify(data);

    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200 || xhr.status === 201) {
                
                    if (sha === null) {
                        stats.solved += 1;
                        stats.easy += difficulty === "Easy" ? 1 : 0;
                        stats.medium += difficulty === "Medium" ? 1 : 0;

                        stats.hard += difficulty === "Hard" ? 1 : 0;
                        successful++;
                        
                    }
                   else{
                       attempted++;
                       tags.push(problemName.tags);
                   }
               
            }
        }
    });
};
/* Function for finding and parsing the full code. */
/* - At first find the submission details url. */
/* - Then send a request for the details page. */
/* - Finally, parse the code from the html reponse. */
/* - Also call the callback if available when upload is success */
function findCode(
    problemName,
    fileName,
    msg,
    action,
    cb = undefined
) {
    /* Get the submission details url from the submission page. */
    var submissionURL;
    const e = document.getElementsByClassName("status-column__3SUg");
    if (checkElem(e)) {
        // for normal problem submisson
        const submissionRef = e[1].innerHTML.split(" ")[1];
        submissionURL =
            "https://leetcode.com" + submissionRef.split("=")[1].slice(1, -1);
    } else {
        // for a submission in explore section
        const submissionRef = document.getElementById("result-state");
        submissionURL = submissionRef.href;
    }

    if (submissionURL != undefined) {
        /* Request for the submission details page */
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                /* received submission details as html reponse. */
                var doc = new DOMParser().parseFromString(
                    this.responseText,
                    "text/html"
                );
                /* the response has a js object called pageData. */
                /* Pagedata has the details data with code about that submission */
                var scripts = doc.getElementsByTagName("script");
                for (var i = 0; i < scripts.length; i++) {
                    var text = scripts[i].innerText;
                    if (text.includes("pageData")) {
                        /* Considering the pageData as text and extract the substring
            which has the full code */
                        var firstIndex = text.indexOf("submissionCode");
                        var lastIndex = text.indexOf("editCodeUrl");
                        var slicedText = text.slice(firstIndex, lastIndex);
                        /* slicedText has code as like as. (submissionCode: 'Details code'). */
                        /* So finding the index of first and last single inverted coma. */
                        var firstInverted = slicedText.indexOf("'");
                        var lastInverted = slicedText.lastIndexOf("'");
                        /* Extract only the code */
                        var codeUnicoded = slicedText.slice(
                            firstInverted + 1,
                            lastInverted
                        );
                        /* The code has some unicode. Replacing all unicode with actual characters */
                        var code = codeUnicoded.replace(
                            /\\u[\dA-F]{4}/gi,
                            function (match) {
                                return String.fromCharCode(
                                    parseInt(match.replace(/\\u/g, ""), 16)
                                );
                            }
                        );
                    
                    }
                }
            }
        };

        xhttp.open("GET", submissionURL, true);
        xhttp.send();
    }
}

/* Util function to check if an element exists */
function checkElem(elem) {
    return elem && elem.length > 0;
}
function convertToSlug(string) {
    const a =
        "àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;";
    const b =
        "aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------";
    const p = new RegExp(a.split("").join("|"), "g");

    return string
        .toString()
        .toLowerCase()
        .replace(/\s+/g, "-") // Replace spaces with -
        .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, "-and-") // Replace & with 'and'
        .replace(/[^\w\-]+/g, "") // Remove all non-word characters
        .replace(/\-\-+/g, "-") // Replace multiple - with single -
        .replace(/^-+/, "") // Trim - from start of text
        .replace(/-+$/, ""); // Trim - from end of text
}
function getProblemNameSlug() {
    const questionElem = document.getElementsByClassName(
        "content__u3I1 question-content__JfgR"
    );
    const questionDescriptionElem = document.getElementsByClassName(
        "question-description__3U1T"
    );
    let questionTitle = "unknown-problem";
    if (checkElem(questionElem)) {
        let qtitle = document.getElementsByClassName("css-v3d350");
        if (checkElem(qtitle)) {
            questionTitle = qtitle[0].innerHTML;
        }
    } else if (checkElem(questionDescriptionElem)) {
        let qtitle = document.getElementsByClassName("question-title");
        if (checkElem(qtitle)) {
            questionTitle = qtitle[0].innerText;
        }
    }
    return convertToSlug(questionTitle);
}

/* Parser function for the question and tags */
function parseQuestion() {
    var questionUrl = window.location.href;
    if (questionUrl.endsWith("/submissions/")) {
        questionUrl = questionUrl.substring(
            0,
            questionUrl.lastIndexOf("/submissions/") + 1
        );
    }
    const questionElem = document.getElementsByClassName(
        "content__u3I1 question-content__JfgR"
    );
    const questionDescriptionElem = document.getElementsByClassName(
        "question-description__3U1T"
    );
    if (checkElem(questionElem)) {
        const qbody = questionElem[0].innerHTML;

        // Problem title.
        let qtitle = document.getElementsByClassName("css-v3d350");
        if (checkElem(qtitle)) {
            qtitle = qtitle[0].innerHTML;
        } else {
            qtitle = "unknown-problem";
        }

        // Problem difficulty, each problem difficulty has its own class.
        const isHard = document.getElementsByClassName("css-t42afm");
        const isMedium = document.getElementsByClassName("css-dcmtd5");
        const isEasy = document.getElementsByClassName("css-14oi08n");

        if (checkElem(isEasy)) {
            difficulty = "Easy";
        } else if (checkElem(isMedium)) {
            difficulty = "Medium";
        } else if (checkElem(isHard)) {
            difficulty = "Hard";
        }
        // Final formatting of the contents of the README for each problem
        const markdown = `<h2><a href="${questionUrl}">${qtitle}</a></h2><h3>${difficulty}</h3><hr>${qbody}`;
        return markdown;
    } else if (checkElem(questionDescriptionElem)) {
        let questionTitle = document.getElementsByClassName("question-title");
        if (checkElem(questionTitle)) {
            questionTitle = questionTitle[0].innerText;
        } else {
            questionTitle = "unknown-problem";
        }

        const questionBody = questionDescriptionElem[0].innerHTML;
        const markdown = `<h2>${questionTitle}</h2><hr>${questionBody}`;

        return markdown;
    }

    return null;
}
