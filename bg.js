
//Listen for messages
chrome.runtime.onMessage.addListener((msg, sender, response) => {
    if (msg.name == "showProblem") {
        //  console.log(msg);
        var discordId = msg.discordId;
        const apiKey = "OUR-API-KEY";
        let url = "http://137.184.148.114:8080/v1/";
        // 696965411047997480
        const apiCall = url + "users/user?discordId="+discordId ;
        fetch(apiCall)
            .then(function (res) {
                res.json().then(function (data) {
                    console.log(data);
                    var enrolledStudyKits = data.enrolledStudyKits;
                    var progress = ((enrolledStudyKits.progress) + 1);
                    response({ word: data.activity, desc: data.type });
                    var views = chrome.extension.getViews({
                        type: "popup",
                    });
                    var problemUrl=url+"studykits/studykit/"+enrolledStudyKits.id;
                    const questions = fetch(
                       problemUrl
                    )
                        .then((response) => response.json())
                        .then((user) => {
                            return user.questions;
                        });

                    const getProblems = async () => {
                        const problems = await questions;
                        var toDoProblems = problems.filter(function (problem) {
                            return problem.dayNumber == progress;
                        });
                        console.log(toDoProblems);
                        toDoProblems.map((problem) => {
                          
                          const a = document.createElement("a");
                          const node = document.createElement("br");
                            var problemUrl =
                                "https://leetcode.com/problems/" + problem.problemId;
                                
                                // const textnode = document.createTextNode(problemUrl);
                                // node.appendChild(textnode);
                                a.target = "_blank";
                                a.href = problemUrl;
                                a.innerText = problem.problemId;
                            x.querySelector(".problem").appendChild(a);
                            x.querySelector(".problem").appendChild(node);
                                
                        });
                      //  console.log(a);
                    };
 
                    getProblems();
                   
                    // todo = problems();
                    // fetch(problemUrl)
                    //   .then(function (res) {
                    //       res.json().then(function (data){
                    //         var questions=data.questions;
                    //         var problems = questions.filter(function (problem) {
                    //             return problem.dayNumber == progress;
                    //         });
                    //         todo=problems;
                    //       })
                    //     })
                   
                    for (var i = 0; i < views.length; i++) {
                        var x = views[i].document.getElementById("myDIV");
                        x.querySelector(".example").innerHTML =
                            "<h4>Study Kit Details:</h4>" +
                            enrolledStudyKits.id;
                        x.querySelector(".example-2").innerHTML =
                            "<h4>Progress:</h4>" +
                            "Day" + progress;
                       
                    }
                });
            })
            .catch(function (err) {
                response({ word: "Error", desc: "Error" });
            });
    }
});